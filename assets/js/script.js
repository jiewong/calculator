const calculatorDisplay = document.querySelector('h1');
const numberBtns = document.querySelectorAll('.number');
const operatorBtns = document.querySelectorAll('.operator');
const decimalBtn = document.querySelectorAll('.decimal');
const clearBtn = document.getElementById('clear-btn');
const clearEntryBtn = document.getElementById('clear-btn-entry');

let firstValue = 0;
let operatorValue = '';
let awaitingNextValue = false;

function calculate(firstNum, secondNum){
  if( operatorValue ==='+'){
    return firstNum + secondNum;
  } else if(operatorValue ==='-'){
    return firstNum - secondNum;
  }
  else if(operatorValue ==='*'){
    return firstNum * secondNum;
  }else if(operatorValue ==='/'){
    return firstNum / secondNum;
  }
  else if(operatorValue ==='='){
    return secondNum;
  }
}

function sendNumberValue(number) {
  // Replace current display value if first value is entered
  if (awaitingNextValue) {
    calculatorDisplay.textContent = number;
    awaitingNextValue = false;
  } else {
    // If current display value is 0, replace it, if not add number to display value
    const displayValue = calculatorDisplay.textContent;
    calculatorDisplay.textContent = displayValue === '0' ? number : displayValue + number;
  }
}

function useOperator(operator) {
  const currentValue = Number(calculatorDisplay.textContent);
  // Prevent multiple operators
  if (operatorValue && awaitingNextValue) {
    operatorValue = operator;
    return;
  }
  // Assign firstValue if no value
  if (!firstValue) {
    firstValue = currentValue;
  } else {
    const calculation = calculate(firstValue, currentValue);
    calculatorDisplay.textContent = calculation;
    firstValue = calculation;
  }
  // Ready for next value, store operator
  awaitingNextValue = true;
  operatorValue = operator;
}

function addDecimal() {
  // If operator pressed, don't add decimal
  if (awaitingNextValue) return;
  // If no decimal, add one
  if (!calculatorDisplay.textContent.includes('.')) {
    calculatorDisplay.textContent = `${calculatorDisplay.textContent}.`;
  }
}

// Reset all values, display
function resetAll() {
  firstValue = 0;
  operatorValue = '';
  awaitingNextValue = false;
  calculatorDisplay.textContent = '0';
}

// clear entry
function clearEntry() {
  if(awaitingNextValue) return;

  calculatorDisplay.textContent = calculatorDisplay.textContent .slice(0,-1);
  
	if (calculatorDisplay.textContent.length < 1){
    calculatorDisplay.textContent = 0;
  }
}

// Event Listener
clearBtn.addEventListener('click', resetAll);

numberBtns.forEach((numberBtn) => {
  numberBtn.onclick = () => sendNumberValue(numberBtn.value);
});

operatorBtns.forEach((operatorBtn) => {
  operatorBtn.onclick = () => useOperator(operatorBtn.value);
})

decimalBtn.forEach((decimal) => {
  decimal.onclick = () => addDecimal();
})

clearEntryBtn.addEventListener('click', ()=> clearEntry());